#include <iostream>
#include <string>

int main(int argc, char **argv) {
    std::cout << "[*] #################################################### [*]" << std::endl;
    std::cout << "[*] Crackme v1.1 by Dan Schmit!" << std::endl;
    std::cout << "[*] This program has been written for educational purposes." << std::endl;
    std::cout << "[*] This is FOSS!" << std::endl;
    std::cout << "[*] #################################################### [*]" << std::endl << std::endl;/*
    std::cout << "[*] Current security features:" << std::endl
                << "    - Algorithm in order to avoid hardcoding password" << std::endl;*/
    std::string givenPassword;
    std::cout << "[*] Enter your licence key: ";
    std::cin >> givenPassword;
    int sum = 0;
    for(char i : givenPassword) sum += (int) i;
    if(sum != 1017) {
        std::cout << "[*] Wrong license key! Please try again." << std::endl;
        return 1;
    }

    std::cout << "[*] License has been activated!" << std::endl;
    std::cout << "[*] Congratulations, you cracked this program succesfully!" << std::endl;
    return 0;
}
