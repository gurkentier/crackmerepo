#include <iostream>
#include <string>
#include <cstdlib>

int getSum(std::string key);

int main(int argc, char **argv) {
    std::cout << "[*] #################################################### [*]" << std::endl;
    std::cout << "[*] Keygen for Crackme v1.1 by Dan Schmit" << std::endl;
    std::cout << "[*] #################################################### [*]" << std::endl << std::endl;
    std::cout << "[*] Generating valid keys for 0x3f9" << std::endl;
    int targetSum = 1017;
    int maxKeys = argc < 2 ? 5 : atoi(argv[1]);
    int currentKeyCount = 0;
    std::string keyString;
    while(currentKeyCount < maxKeys) {
        int currentSum = 0;
        const char randomChar = 'A' + rand() % ('z' - 'A' + 1);
        keyString += randomChar;
        currentSum = getSum(keyString);
        if(currentSum > targetSum) {
            keyString = "";
            //std::cout << "[*] Key too big, resetting..." << std::endl;
        }
        if(currentSum == targetSum) {
            std::cout << "[*] Found valid key: " << keyString << std::endl;
            currentKeyCount++;
        }
    }

    return 0;
}

int getSum(std::string key) {
    int sum = 0;
    for(char i : key) {
        sum += (int) i;
    }

    return sum;
}