import hashlib
import secrets
import sys


def gen(name):
    return hashlib.sha1(name.encode()).hexdigest().upper()


def generateLicenseFile(name, serial):
    file = open(name + ".foxlicense", "w+")
    file.write("<?xml version\"1.0\" encoding=\"UTF-8\"?>")
    file.write("<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" "
               "\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">")
    file.write("<plist version=\"1.0\">")
    file.write("<dict>")
    file.write("	<key>regName</key>")
    file.write("	<string>" + name + "</string>")
    file.write("	<key>regNumber</key>")
    file.write("	<string>" + serial + "</string>")
    file.write("</dict>")
    file.write("</plist>")


if __name__ == "__main__":
    print("[*] ###########################################")
    print("[*] Fox Challenge Keygen by Dan Schmit v0.1")
    print("[*] Generating valid name and serial pairs...")
    print("[*] ###########################################")
    currentKeys = 0
    maxKeys = int(sys.argv[1]) if len(sys.argv) > 1 else 5
    while currentKeys < maxKeys:
        name = secrets.token_hex(5)
        serial = gen(name)
        print("[*] Found valid serial for name " + name + " - " + serial)
        generateLicenseFile(name, serial)
        currentKeys += 1
