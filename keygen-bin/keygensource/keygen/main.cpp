#include <iostream>
#include <cstring>

int getSum(const char *key);
bool check_key(const char *key);

int main(int argc, char **argv) {
    std::cout << "[*] ########################################## [*]" << std::endl;
    std::cout << "[*] Keygen for keygen.bin by Dan Schmit v0.1" << std::endl;
    std::cout << "[*] Generating valid keys..." << std::endl;
    std::cout << "[*] ########################################## [*]" << std::endl;
    int maxKeys = argc < 2 ? 5 : atoi(argv[1]);
    int currentKeys = 0;
    std::string currentKey;

    while (currentKeys < maxKeys) {
        const char randomChar = 'A' + rand() % ('z' - 'A' + 1);
        currentKey += randomChar;
        //std::cout << currentKey << std::endl;
        //std::cout << "[*] DEBUG: current sum - " << getSum(currentKey.c_str()) << std::endl;
        //std::cout << "[*] DEBUG: current len - " << strlen(currentKey.c_str()) << std::endl;
        //std::cout << "[*] DEBUG: check res - " << check_key(currentKey.c_str()) << std::endl;
        if (std::strlen(currentKey.c_str()) > 10) currentKey = "";
        else if(check_key(currentKey.c_str())) {
            std::cout << "[*] Found valid key: " << currentKey << std::endl;
            currentKeys++;
        }
    }

    return 0;
}

int getSum(const char *key) {
    int sum = 0;
    for(int i = 0; i < strlen(key); i++) sum += key[i];
    return sum;
}

bool check_key(const char *key)
{
    if ( strlen(key) <= 7 || strlen(key) > 10) return false;
    return getSum(key) > 999;

}
