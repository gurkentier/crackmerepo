import secrets
import sys


def validate(key):
    valid = True
    key = str(key)
    if len(key) == 0x13:
        key = key.split("-")
        if len(key) == 4 and len(key[0]) == 4 and len(key[1]) == 4:
            if len(key[2]) == 4:
                if len(key[3]) == 4:
                    sumOfFirstAndSecond = int(key[0]) + int(key[1])
                    #print("6597 - sum / 2: " + str(6597 - (sumOfFirstAndSecond / 2)))
                    valid = True if 6597 - (sumOfFirstAndSecond >> 2) == int(key[3]) else False
                else:
                    valid = False
            else:
                valid = False
        else:
            valid = False

    else:
        valid = False


    return valid



def gen():
    keyList = ["", "", "", ""]
    keyList[0] = sizeExtend(secrets.randbelow(3298))
    keyList[1] = sizeExtend(secrets.randbelow(3297))
    if (int(keyList[0]) + int(keyList[1])) % 2 == 1:
        keyList[1] = sizeExtend(int(keyList[1]) - 1)
    keyList[3] = sizeExtend(int(6597 - ((int(keyList[0]) + int(keyList[1])) >> 2)))
    keyList[2] = "HACK"
    return keyList[0] + "-" + keyList[1] + "-" + keyList[2] + "-" + keyList[3]


def sizeExtend(val):
    val = val if val > 0 else val * -1
    if 9999 >= val >= 1000:
        return str(val)
    elif 999 >= val >= 100:
        return "0" + str(val)
    elif 99 >= val >= 10:
        return "00" + str(val)
    elif 9 >= val >= 0:
        return "000" + str(val)


if __name__ == "__main__":
    maxKeys = int(sys.argv[1]) if len(sys.argv) > 1 else 5
    currentKeys = 0
    print("[*] ############################################")
    print("[*] Sandwich Challenge Keygen by Dan Schmit v1")
    print("[*] Generating " + str(maxKeys) + " random keys...")
    print("[*] ############################################\n")
    while currentKeys < maxKeys:
        key = gen()
        if validate(key):
            print("[*] Found valid key: " + key)
            currentKeys += 1


