# /* @class UnicornAppDelegate */
# -(bool)validateSerial:(void *)arg2 forName:(void *)arg3 {
# eax = [arg3 stringByAppendingString:@"+unicorn"];
# eax = [eax md5HexHash];
# eax = [eax uppercaseString];
# eax = [eax substringToIndex:0x14];
# eax = [eax isEqualToString:arg2];
# eax = eax != 0x0 ? 0x1 : 0x0;
# return eax;
# }

import hashlib
import secrets
import sys


def generateSerial(name):
    eax = name + "+unicorn"
    eax = hashlib.md5(eax.encode()).hexdigest()
    return eax[:20].upper()


def keygen(name):
    print("[*] Generating serial numbers for name: " + name)
    print("[*] Found valid serial number: " + generateSerial(name))


if __name__ == "__main__":
    print("[*] ############################################")
    print("[*] Unicorn Challenge Keygen by Dan Schmit v1")
    print("[*] Generating valid license credentials...")
    print("[*] ############################################\n")
    maxKeys = int(sys.argv[1]) if len(sys.argv) > 1 else 5
    for x in range(maxKeys):
        keygen(secrets.token_hex(4))

